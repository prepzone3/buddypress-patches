<?php
/**
 * Основные параметры WordPress.
 *
 * Этот файл содержит следующие параметры: настройки MySQL, префикс таблиц,
 * секретные ключи и ABSPATH. Дополнительную информацию можно найти на странице
 * {@link http://codex.wordpress.org/Editing_wp-config.php Editing wp-config.php}
 * Кодекса. Настройки MySQL можно узнать у хостинг-провайдера.
 *
 * Этот файл используется скриптом для создания wp-config.php в процессе установки.
 * Необязательно использовать веб-интерфейс, можно скопировать этот файл
 * с именем "wp-config.php" и заполнить значения вручную.
 *
 * @package WordPress
 */

// ** Параметры MySQL: Эту информацию можно получить у вашего хостинг-провайдера ** //
/** Имя базы данных для WordPress */
define('DB_NAME', 'prepzone');

/** Имя пользователя MySQL */
define('DB_USER', 'root');

/** Пароль к базе данных MySQL */
define('DB_PASSWORD', '');

/** Имя сервера MySQL */
define('DB_HOST', 'localhost');

/** Кодировка базы данных для создания таблиц. */
define('DB_CHARSET', 'utf8');

/** Схема сопоставления. Не меняйте, если не уверены. */
define('DB_COLLATE', '');

/**#@+
 * Уникальные ключи и соли для аутентификации.
 *
 * Смените значение каждой константы на уникальную фразу.
 * Можно сгенерировать их с помощью {@link https://api.wordpress.org/secret-key/1.1/salt/ сервиса ключей на WordPress.org}
 * Можно изменить их, чтобы сделать существующие файлы cookies недействительными. Пользователям потребуется авторизоваться снова.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'V]ntX1~0rq=4XVBT7u~z[vPU7=KhLT.i&y7A|q*i82$!^|EZc)haf^6)@)YG&$GT');
define('SECURE_AUTH_KEY',  'GtaN<7(8MjN6+-JA/3nI{{MFA<|;Ph_g-Gd^}RjE&p=nFezk9o@].OOlIBxgie+j');
define('LOGGED_IN_KEY',    '@U|OoPq)z9W>i+g4b(D>]QmA*f(oA#+ifvSvcU6q-oh:pX|a:CK8*]pTk:h5y-#w');
define('NONCE_KEY',        'qPy*9iCiCvb U;$+W4BNH9Iqbg|ukR-yMGSPbhGw77oi8=@L:fo}=1VyExagxAM-');
define('AUTH_SALT',        '|$nyWX7:-[X8Ip2+ g3HW{WwnxdvypJ;#HO(a<wi..|o4{dncn2=qJ- e)7RD~qu');
define('SECURE_AUTH_SALT', '.AB^wsaLOo:7:^`Z>n*(7 dVvM{FvAIy*:iN7zSxKSIdN(+:u{khr)a{Fpf) ]9k');
define('LOGGED_IN_SALT',   'e-M`E}(p$a0(qZt0vI~&2j(o-nPw}#B[J]A#Ed@sxpy(32DTDI}:+M(q#y;_`Dh^');
define('NONCE_SALT',       'Y0^ Zpk{OfzFW,g+H#z/cVVy+%UvC1LR/u;++e;y &|)]2a/L9bX?[G0ipxoaeL$');

/**#@-*/

/**
 * Префикс таблиц в базе данных WordPress.
 *
 * Можно установить несколько сайтов в одну базу данных, если использовать
 * разные префиксы. Пожалуйста, указывайте только цифры, буквы и знак подчеркивания.
 */
$table_prefix  = 'wp_';

/**
 * Для разработчиков: Режим отладки WordPress.
 *
 * Измените это значение на true, чтобы включить отображение уведомлений при разработке.
 * Разработчикам плагинов и тем настоятельно рекомендуется использовать WP_DEBUG
 * в своём рабочем окружении.
 */
define('WP_DEBUG', false);

/* Это всё, дальше не редактируем. Успехов! */

/** Абсолютный путь к директории WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Инициализирует переменные WordPress и подключает файлы. */
require_once(ABSPATH . 'wp-settings.php');
